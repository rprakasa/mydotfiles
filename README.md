### mydotfiles  
  
## My application list  
  
# Sway  
OS = [Langitketujuh](https://langitketujuh.id)  
Terminal = kitty  
Application launcher = wofi  
Screenshot = grim, swappy, slurp  
Brightness controller = brightnessctl  
Audio server = pipewire, wireplumber  
Player control = playerctl  
Wallpaper = azote  

# Waybar  
Status-shalat = https://gitlab.com/nesstero/status-waktu-shalat

## Screenshot
![Screenshot](Screenshot.png)
